PChome 商店街，爬蟲實作
======


題目
------
+ <https://www.pcstore.com.tw/>

```
https://www.pcstore.com.tw/ 的爬蟲，能夠有個 function輸入關鍵字搜索，
並且回傳一頁的標題就可以。

請盡可能以class的方式來撰寫。
```

<br>

研究
------
### 搜尋「藍芽」網址: 
<https://www.pcstore.com.tw/adm/search.htm?store_k_word=JUU4JTk3JThEJUU4JThBJUJE&slt_k_option=1&ftd_p=>

+ 關鍵字 store_k_word 有用 Base64 轉碼

<br>
   
### 結果的資料內容，使用 Ajax 請求以下路徑 :
```
https://www.pcstore.com.tw/adm/api/get_search_data.php?
store_k_word = JUU4JTk3JThEJUU4JThBJUJE & t = 1631908774212 & k_option = &slt_p_range_s = &slt_p_range_e =
&st_sort = &s_page = &ft_mk = &pch_exh = &pch_pa = &no_exh_flag = 0
```

+ 其中的 t 參數，還使用 Date.now() 動態產生，不然請求會出錯


<br>

運行
------
```py
python3 app_main.py 
```

<br>

輸出
------
+ result.json => 取得的 JSON 資料
+ title => 一頁的標題內容

<br>

### 有圖有真相
**PC Home : 搜尋「藍芽」**

![001](assets/001.pc_home.png)


**result.json**
![002](assets/002.result-json.png)


**003.title-txt**
![003](assets/003.title-txt.png)






