import requests
import time
import json
import urllib
import base64
from datetime import datetime


about = """
題目：
    https://www.pcstore.com.tw/ 的爬蟲，
    能夠有個 function輸入關鍵字搜索，
    並且回傳一頁的標題就可以。
    請盡可能以class的方式來撰寫。
"""

impl = """
研究 : 
    搜尋網址:
        https://www.pcstore.com.tw/adm/search.htm?store_k_word=JUU4JTk3JThEJUU4JThBJUJE&slt_k_option=1&ftd_p=

    關鍵字 store_k_word 有用 Base64 轉碼
    
    結果的資料內容，使用 Ajax 請求以下路徑 :
        https://www.pcstore.com.tw/adm/api/get_search_data.php?
        store_k_word = JUU4JTk3JThEJUU4JThBJUJE & t = 1631908774212 & k_option = &slt_p_range_s = &slt_p_range_e =
        &st_sort = &s_page = &ft_mk = &pch_exh = &pch_pa = &no_exh_flag = 0

    其中的 t 參數，還使用 Date.now 動態產生，不然請求會出錯

輸出 :
    result.json => 取得的 JSON 資料
    title => 一頁的標題內容
"""


class Collector:
    def __init__(self, keyword, page):
        self.keyword = keyword
        self.page = page

    def __str__(self):
        return about + "\n\n" + impl

    def query(self, keyword):
        keyword_code = self.encodeKeyword(keyword)

        print("嘗試請求關鍵字 : {}".format(self.keyword))

        param_time = int(datetime.now().timestamp() * 1000)
        remote_url = ('https://www.pcstore.com.tw/adm/api/get_search_data.php/adm/api/get_search_data.php?'
                      'store_k_word={}&t={}&k_option=&slt_p_range_s=&slt_p_range_e='
                      '&st_sort=&s_page=&ft_mk=&pch_exh=&pch_pa=&no_exh_flag=0').format(keyword_code, param_time)

        try:  # 做一個例外處理，如果出現錯誤可以避免卡住
            rs = requests.get(url=remote_url)
            rs.encoding = "UTF-8"

            # 輸出結果
            data = rs.text
            with open("result.json", 'w', encoding="utf-8") as f:
                f.write(data)  # 寫入
            print("已輸出 result.json ")

            json_data = json.loads(data)
            all_title = []  # 先宣告一個 所有商品的標題

            lst_prod = json_data['prod']  # 取得商品列表
            for prod in lst_prod:
                all_title.append(prod['title'])  # 將商品保存進商品陣列

            with open("title.txt", 'w', encoding="utf-8") as f:
                for title in all_title:
                    f.write("%s\n" % title)
            print("已輸出 title.txt ")
        except Exception as e:  # 輸出的錯誤名稱叫e
            print(e)  # 印出錯誤內容
            print(data)  # 印出這一頁所抓取的值

    def encodeKeyword(self, keyword):
        encodeURI = urllib.parse.quote(keyword)  # encodeURIComponent
        text = encodeURI.encode("UTF-8")
        code = base64.b64encode(text)
        code = code.decode("UTF-8")
        return code
